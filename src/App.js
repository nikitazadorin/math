import './App.css';
import Spline3d from './spline3d/Spline3d'

function App() {
  return (
    <Spline3d />
  );
}

export default App;
